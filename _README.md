# {{project-name}}

*TODO*


{% if pipeline == 'TAG_TO_PUBLISH' %}
# CI / CD Artifacts

## Tag-To-Publish Stable

Push a tag in the form of v${VERSION} to release a stable version of the crate.

## Beta Artifacts

All Development branches have manual jobs for publishing a beta package.
{% else if pipeline == 'DEFAULT_TEMPLATE' %}
# CI / CD Artifacts

Artifacts are generated and deployed per [the default TPFS-CI Rust pipeline template](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci/-/tree/master/templates/rust).
{% endif %}