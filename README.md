# TPFS-crate

TPFS-crate is a [cargo-generate](https://github.com/cargo-generate/cargo-generate) template for engineers to quickly spin up new lib or bin Rust crates.  The template represents initial best practice states for linting, CI/CD, gitignore, errors, and more.


# How to use TPFS-crate template

## Install `cargo-generate`:

```bash
cargo install cargo-generate
```
(For more in-depth instructions like installing a vendored version of openssl alongside, see the [documentation for cargo-generate](https://github.com/cargo-generate/cargo-generate))

## Generate a new project:

```bash
cargo generate --git git@gitlab.com:TransparentIncDevelopment/gitlab_project_templates/tpfs-crate.git --name <new_crate_name> -i $HOME/.ssh/<private_keyfile_name>
```

**Pass `--lib` to create a library-only crate.**

NOTE: Your ssh key cannot be password protected for cargo-generate to be able to use it correctly (https://github.com/cargo-generate/cargo-generate/issues/384).  If you only want to use a password-protected ssh key you should clone this template locally and run:

```bash
cargo generate --git <path/to/cloned/repo> --name <new_crate_name>
```

# Convenience Features

To make this template usable via the command `cargo generate tpfs`:

`cargo-generate` allows you to add "favorite" templates to a file called `$CARGO_HOME/cargo-generate.toml`.  See [here](https://cargo-generate.github.io/cargo-generate/favorites.html) for details on favorites and [here](https://cargo-generate.github.io/cargo-generate/usage.html#ssh-identity-file-private-key) for details on using ssh with it.

Add the following to `$CARGO_HOME/cargo-generate.toml`:

```toml
# $CARGO_HOME/cargo-generate.toml

[defaults]
ssh_identity = "/path/to/gitlab/ssh"

[favorites.tpfs]
git = "https://gitlab.com/TransparentIncDevelopment/gitlab_project_templates/tpfs-crate"
branch = "master"
```

If you have a password set for ssh then cargo-generate will not support it, and you should pull the project down to point to the local git directory:


```toml
# $CARGO_HOME/cargo-generate.toml

[favorites.tpfs]
git = "/absolute/path/to/local/cloned/directory"
branch = "master"
```
